# README / ЧАВО #

Simple "dual limb" widget. Test project for small IT-company.

Элемент графического интерфейса пользователя, имитирующий сдвоенный лимб. Тестовое задание для одной небольшой компании.

![screenshot / экранный снимок](https://bitbucket.org/slava_phirsov/limb/raw/master/screenshot.png)

### What is this repository for? / Что тут к чему ###


Widget consists of two co-axial round scales (major and minor), controled by one knob. User can rotate knob by mouse movement. Is based on Qt Graphics View Framework.

Элемент графического интерфейса пользователя представляет собой две соосные круговые шкалы, управляемые одной ручкой. Пользователь воздействует на ручку перемещением мыши. Виджет основан на инфраструктуре Graphics View библиотеки Qt.


### Contribution guidelines / Как бы помочь ###

Do nothing.

Просто не мешайте. Этот проект сделан "для себя", в чисто познавательных целях.
