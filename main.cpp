#include <QApplication>

#include <QGraphicsWidget>

#include <QGraphicsView>
#include <QGraphicsScene>

#include <QEvent>
#include <QGraphicsSceneMouseEvent>

#include <QPixmap>
#include <QGraphicsPixmapItem>

#include <cmath>


namespace
{
qreal CircleDeg = 360.0;

qreal square(qreal x)
{
    return x * x;
}

qreal distance(const QPointF& p1, const QPointF& p2)
{
    return std::sqrt(square(p1.x() - p2.x()) + square(p1.y() - p2.y()));
}

qreal azimuth(const QPointF& p1, const QPointF& p2)
{
    return std::atan2(p2.y() - p1.y(), p2.x() - p1.x());
}

qreal radToDeg(qreal rad)
{
    return rad / asin(1) * CircleDeg / 4;
}

qreal normalizeDeg(qreal deg)
{
    deg = std::fmod(deg, CircleDeg);

    return
        deg > CircleDeg / 2 ?
            deg - CircleDeg :
            deg < -CircleDeg / 2 ?
                deg + CircleDeg :
                deg;
}
}


class LimbGraphicsWidget: public QGraphicsWidget
{
    Q_OBJECT
public:

    LimbGraphicsWidget(QGraphicsItem* parent=0, Qt::WindowFlags =0);

    qreal value() const;

signals:
    void valueChanged(qreal);

protected:
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent*);

private:

    static qreal CogRate;

    void rotateKnob(qreal);

    QGraphicsPixmapItem* createChild(const QPixmap&);

    QGraphicsPixmapItem *minor_, *major_, *knob_;
    qreal knob_rotation_, value_;
};


qreal LimbGraphicsWidget::CogRate = 36.0;


LimbGraphicsWidget::LimbGraphicsWidget(
    QGraphicsItem* parent, Qt::WindowFlags flags
):
    QGraphicsWidget(parent, flags),
    minor_(0), major_(0), knob_(0),
    knob_rotation_(0.0), value_(0.0)
{
    minor_ = createChild(QPixmap(":/minor.png"));
    major_ = createChild(QPixmap(":/major.png"));

    QGraphicsPixmapItem* panel = createChild(QPixmap(":/panel.png"));

    knob_ = createChild(QPixmap(":/knob.png"));

    QPointF limb_axle(
        panel->boundingRect().center().x(),
        panel->boundingRect().top() + major_->boundingRect().height() / 2 + 20
    );

    QPointF knob_axle(
        panel->boundingRect().center().x(),
        panel->boundingRect().bottom() -
        20 -
        knob_->boundingRect().height() / 2
    );

    minor_->setPos(limb_axle - minor_->boundingRect().center());
    major_->setPos(limb_axle - major_->boundingRect().center());
    knob_->setPos(knob_axle - knob_->boundingRect().center());

    setFocusPolicy(Qt::StrongFocus);
    setFlags(ItemIsSelectable | ItemIsFocusable);

    setSizePolicy(
        QSizePolicy::Fixed, QSizePolicy::Fixed,
        QSizePolicy::DefaultType
    );

    setGeometry(panel->boundingRect());
}

qreal LimbGraphicsWidget::value() const
{
    return value_;
}

void LimbGraphicsWidget::mouseMoveEvent(QGraphicsSceneMouseEvent* mouse_event)
{
    using std::sqrt;

    qreal knob_radius = knob_->boundingRect().width() / 2;

    QPointF
        knob_center = knob_->mapToParent(knob_->boundingRect().center()),
        last_pos = mouse_event->lastPos(),
        pos = mouse_event->pos();

    if (
        distance(knob_center, last_pos) < knob_radius &&
        distance(knob_center, pos) < knob_radius
    )
        rotateKnob(
            radToDeg(azimuth(knob_center, pos) - azimuth(knob_center, last_pos))
        );
}

void LimbGraphicsWidget::rotateKnob(qreal angle)
{
    using std::isfinite;

    angle = normalizeDeg(angle);

    qreal
        knob_rotation = normalizeDeg(knob_rotation_ + angle),
        value = normalizeDeg(value_ + angle / CogRate)
    ;

    if (isfinite(knob_rotation) && isfinite(value))
    {
        knob_->setRotation(knob_rotation);
        minor_->setRotation(knob_rotation);
        major_->setRotation(value);

        knob_rotation_ = knob_rotation;
        value_ = value;

        emit valueChanged(value_);
    }
}

QGraphicsPixmapItem* LimbGraphicsWidget::createChild(const QPixmap& pixmap)
{
    QGraphicsPixmapItem* result = new QGraphicsPixmapItem(pixmap, this);

    result->setShapeMode(QGraphicsPixmapItem::BoundingRectShape);
    result->setTransformOriginPoint(result->boundingRect().center());
    result->setTransformationMode(Qt::SmoothTransformation);

    return result;
}


int main (int argc, char* argv[])
{
    QApplication app(argc, argv);

    QGraphicsScene scene;

    LimbGraphicsWidget* limb = new LimbGraphicsWidget();

    scene.addItem(limb);
    scene.setFocusItem(limb);
    scene.setStickyFocus(true);

    QGraphicsView view(&scene);

    view.setFocusPolicy(Qt::StrongFocus);
    view.centerOn(limb);
    view.show();

    return app.exec();
}


#include "main.moc"
